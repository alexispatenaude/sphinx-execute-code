#!/usr/bin/env/python
"""
sphinx-execute-code module for execute_code directive
To use this module, add: extensions.append('sphinx_execute_code')

Available options:

        'linenos': directives.flag,
        'output_language': directives.unchanged,
        'input_language': directives.unchanged,
        'hide_code': directives.flag,
        'hide_headers': directives.flag,
        'hide_results': directives.flag,
        'filename': directives.path,
        'hide_filename': directives.flag,
        'hidden_args': directives.unchanged,
        'extra_args': directives.unchanged,
        'skip_exists': directives.unchanged,

Usage:

.. example_code:
   :linenos:
   :hide_code:

   print 'Execute this python code'

   See Readme.rst for documentation details
"""
import sys
import subprocess
from io import StringIO
import os
from docutils.parsers.rst import Directive, directives
from docutils import nodes
import shlex
# import pdb

# execute_code function thanks to Stackoverflow code post from hekevintran
# https://stackoverflow.com/questions/701802/how-do-i-execute-a-string-containing-python-code-in-python

__author__ = 'jp.senior@gmail.com'
__docformat__ = 'restructuredtext'
__version__ = '0.2a2'

EXPECTED_RETURN_CODE = 0

class ExecuteCode(Directive):
    """ Sphinx class for execute_code directive
    """
    has_content = True
    required_arguments = 0
    optional_arguments = 11

    option_spec = {
        'linenos': directives.flag,
        'output_language': directives.unchanged,  # Runs specified pygments lexer on output data
        'input_language': directives.unchanged,
        'hide_code': directives.flag,
        'hide_headers': directives.flag,
        'hide_results': directives.flag,
        'filename': directives.path,
        'hide_filename': directives.flag,
        'hidden_args': directives.unchanged,
        'extra_args': directives.unchanged,
        'skip_exists': directives.unchanged,
    }

    @classmethod
    def execute_code(cls, code, input_language=None, expected_return_code=EXPECTED_RETURN_CODE):
        """ Executes supplied code as pure python and returns a list of stdout, stderr

        Args:
            code (string): Python code to execute

            input_language (string): Either None ( or 'python') or 'bash'

        Results:
            (list): stdout, stderr of executed python code

        Raises:
            ExecutionError when supplied python is incorrect

        Examples:
            >>> execute_code('print "foobar"')
            'foobar'
        """

        output = StringIO()
        err = StringIO()

        if input_language is None or input_language == 'python':
            sys.stdout = output
            sys.stderr = err

            try:
                # pylint: disable=exec-used
                exec(code)
            # If the code is invalid, just skip the block - any actual code errors
            # will be raised properly
            except TypeError:
                pass
            sys.stdout = sys.__stdout__
            sys.stderr = sys.__stderr__

            output = output.getvalue()
            err    = err.getvalue()
        elif input_language == 'bash':
            code_elements_dirty = shlex.split(code)

            code_elements = [x.strip() for x in code_elements_dirty]

            # print("Executing %s\nAs list: %s" % (code, str(code_elements)))

            process = subprocess.Popen(code_elements,
                                       stdout=subprocess.PIPE,
                                       #stderr=subprocess.PIPE,
                                       )
            output, err = process.communicate()
            output = output.decode('iso-8859-1')

            try:
                err    = err.decode('iso-8859-1')
            except AttributeError:
                err = ''
            # end try

            if process.returncode != expected_return_code:
                sys.stderr.write(err)
                raise Exception('code "%s" returned unexpected code %d' % (code, process.returncode))
            # end if
        else:
            raise Exception('Unhandled input_language %s' % input_language)
        # end if

        results = list()
        results.append(output)
        results.append(err)
        results = ''.join(results)

        return results
    # end execute_code()

    def run(self):
        """ Executes python code for an RST document, taking input from content or from a filename

        :return:
        """
        env = self.state.document.settings.env  # sphinx.environment.BuildEnvironment
        config = env.config  # sphinx.config.Config
        skip_execute_code = config["skip_execute_code"]

        calling_source_directory = os.path.abspath(os.path.join((env.doc2path(env.docname)), os.pardir))

        language = self.options.get('language') or 'python'
        output_language = self.options.get('output_language') or 'none'
        input_language  = self.options.get('input_language')  or 'none'
        filename = self.options.get('filename')
        hidden_args = self.options.get('hidden_args') or ''
        extra_args = self.options.get('extra_args') or ''
        skip_exists = self.options.get('skip_exists') or ''
        code = ''

        if not filename:
            code = '\n'.join(self.content)
        # end if

        if filename:
            try:
                with open(filename, 'r') as code_file:
                    code = code_file.read()
                    self.warning('code is %s' % code)
                # end with
            except (IOError, OSError) as err:
                # Raise warning instead of a code block
                error = 'Error opening file: %s, working folder: %s' % (err, os.getcwd())
                self.warning(error)
                return [nodes.warning(error, error)]
            # end try
        # end if

        output = []

        # Show the example code
        if not 'hide_code' in self.options:
            display_code = code

            if extra_args:
                display_code += ' %s' % extra_args
            # end if

            input_code = nodes.literal_block(display_code, display_code)

            input_code['language'] = language
            input_code['linenos'] = 'linenos' in self.options

            if not 'hide_headers' in self.options:
                suffix = ''

                if not 'hide_filename' in self.options:
                    suffix = '' if filename is None else str(filename)
                # end if

                output.append(nodes.caption(text='Code %s' % suffix))
            # end if

            output.append(input_code)
        # end if

        # Show the code results
        if not 'hide_headers' in self.options:
            output.append(nodes.caption(text='Results'))
        # end if

        if skip_exists:
            missing_artifact = False

            for file in shlex.split(skip_exists):
                artifact_exists = os.path.exists(os.path.join(calling_source_directory, file))

                if not artifact_exists:
                    print('Missing artifact:', file, os.path.realpath(file))
                    # print('path_execute_code:', path_execute_code)
                # end if

                missing_artifact = missing_artifact or not artifact_exists
            # end for
        else:
            missing_artifact = True
        # end if

        if not skip_execute_code and missing_artifact:
            code_command = code + ' ' + hidden_args

            print('Executing %s in %s' % (code_command, env.doc2path(env.docname)))

            code_results = self.execute_code(code_command, input_language=input_language)
            code_results = nodes.literal_block(code_results, code_results)

            code_results['linenos'] = 'linenos' in self.options
            code_results['language'] = output_language
        else:
            code_results = None
        # end if

        if not 'hide_results' in self.options and code_results is not None:
            output.append(code_results)
        # end if

        return output
    # end run()
# end class ExecuteCode

def on_html_page_context(app, pagename, templatename, context, doctree):
    if doctree:
        print('doctree.attributes[\'source\']:', doctree.attributes['source']) # Path to .rst file

def setup(app):
    """ Register sphinx_execute_code directive with Sphinx """
    app.add_directive('execute_code', ExecuteCode)
    app.add_config_value('skip_execute_code', False, 'html')
    # app.connect('html-page-context', on_html_page_context)

    return {'version': __version__}
# end setup()